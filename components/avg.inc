<?php

/**
 * @file
 * Webform module uuid reference component.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_avg() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => 0,
    'extra' => array(
      'disabled' => 1,
      'title_display' => 0,
      'description' => '',
      'attributes' => array(),
      'private' => FALSE,
      'display_mode' => array(),
    ),
  );
}

/**
 * Implements _webform_theme_component().
 */
function _webform_theme_avg() {
  return array(
    'webform_display_avg' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_avg($component) {
  $form = array();
  $form['display']['#access'] = FALSE;
  return $form;
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_avg($component, $value = NULL, $filter = TRUE) {
  $options = array();
  $options["1"] = t("Agreed");

  $element = array(
    '#title' => $component['name'],
    '#type' => 'checkboxes',
    '#options' => $options,
    '#weight' => $component['weight'],
  );

  return $element;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_avg($component, $value, $format) {
  return array(
    '#title' => $component['name'],
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
    '#weight' => $component['weight'],
    '#theme' => 'webform_display_avg',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#format' => $format,
    '#value' => $value,
    '#translatable' => array('title', 'field_prefix', 'field_suffix'),
  );
}

/**
 * Format the output of data for this component.
 */
function theme_webform_display_avg($variables) {
  $view = t('not agreed');

  $element = $variables['element'];

  if(isset($element['#value']) && $element['#value']["1"] == 1) {
    $view = t('agreed');
  }

  return $view;
}